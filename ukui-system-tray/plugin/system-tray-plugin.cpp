#include "system-tray-plugin.h"
#include "tray-items-model.h"
#include "item-group-model.h"
#include "status-notifier-host.h"
#include "tray-item.h"

#include <QQmlEngine>
#include <QQmlContext>

void SystemTrayPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("org.ukui.systemTray"));
    qmlRegisterUncreatableType<ItemGroupModel>(uri,1,0,"ItemGroupModel", "");

    qmlRegisterSingletonType<TrayItemsModel>(uri,1,0,"ItemModel", [] (QQmlEngine *, QJSEngine *) -> QObject* {
        return TrayItemsModel::instance();
    });
    qmlRegisterSingletonType<ItemGroupModel>(uri,1,0,"ShowModel", [] (QQmlEngine *, QJSEngine *) -> QObject* {
        ItemGroupModel* showModel = new ItemGroupModel();
        showModel->setModelType(ItemGroupModel::Show);
        return showModel;
    });
    qmlRegisterSingletonType<ItemGroupModel>(uri,1,0,"FoldModel", [] (QQmlEngine *, QJSEngine *) -> QObject* {
        ItemGroupModel* foldModel = new ItemGroupModel();
        foldModel->setModelType(ItemGroupModel::Fold);
        return foldModel;
    });
}
