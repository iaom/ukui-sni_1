#ifndef ITEMGROUPMODEL_H
#define ITEMGROUPMODEL_H

#include <QSortFilterProxyModel>
#include <QPersistentModelIndex>
#include <QHash>
#include <QObject>
#include <QStringList>

#include "tray-item.h"

class TrayItemsModel;

class ItemGroupModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    enum GroupType {Show, Fold};
    Q_ENUM(GroupType)

    explicit ItemGroupModel(QObject *parent = nullptr);
    void setModelType(GroupType type);
    GroupType getModelType();

    QVariant data(const QModelIndex &index, int role) const override;

    Q_INVOKABLE void onActivate(const QModelIndex &index);
    Q_INVOKABLE void onContextMenu(const QModelIndex &index);

    Q_INVOKABLE QVariant foldIcon();

    Q_INVOKABLE int groupBegin() const;
    Q_INVOKABLE int groupEnd() const;

protected:
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

private:

    QHash<QPersistentModelIndex, int> m_order;
    TrayItemsModel *m_sourceModel = nullptr;
    int m_groupBegin = 0;
    int m_groupEnd = 0;
    GroupType m_groupType = GroupType::Show;
};

#endif // ITEMGROUPMODEL_H
