#include "tray-items-model.h"
#include "item-group-model.h"
#include <QMetaEnum>
#include <QDebug>
#include <config-loader.h>

static TrayItemsModel* m_instance = nullptr;
static const QStringList DEFAULT_ORDERED_ITEMS = {"ukui-bluetooth",
                                                  "ukui-power-manager-tray",
                                                  "ukui-search",
                                                  "ukui-volume-control-applet-qt",
                                                  "kylin-nm"};
static const QStringList DEFAULT_FIXED_ITEMS = {"ukui-sidebar"};
TrayItemsModel *TrayItemsModel::instance()
{
    if (!m_instance) {
        m_instance = new TrayItemsModel();
    }
    return m_instance;
}

TrayItemsModel::TrayItemsModel(QObject *parent) : QAbstractListModel(parent)
{
    if (!m_settings) {
        m_settings = UkuiQuick::ConfigLoader::getGlobalWidgetConfig("org.ukui.systemTray")->settings();
    }
    //get separateIndex
    if(!m_settings->contains(QStringLiteral("separateIndex"))) {
        m_settings->setValue(QStringLiteral("separateIndex"), m_separateIndex);
    } else {
        m_separateIndex = m_settings->value("separateIndex").toInt();
    }
    //fixed items
    if(!m_settings->contains(QStringLiteral("fixedItems"))) {
        m_fixList = DEFAULT_FIXED_ITEMS;
        m_settings->setValue(QStringLiteral("fixedItems"), DEFAULT_FIXED_ITEMS);
    } else {
        m_fixList = m_settings->value("fixedItems").toStringList();
    }
    //showed items
    if(!m_settings->contains(QStringLiteral("orderedItems"))) {
        m_orderList = DEFAULT_ORDERED_ITEMS;
        m_settings->setValue(QStringLiteral("orderedItems"), DEFAULT_ORDERED_ITEMS);
    } else {
        m_orderList = m_settings->value(QStringLiteral("orderedItems")).toStringList();
    }

    //hide items
    m_hideList = m_settings->value(QStringLiteral("trayIconsInhibited")).toStringList();

    //固定的items放在order items的前面
    m_orderList = m_fixList + m_orderList;

    m_sniHost = UkuiSni::StatusNotifierHost::self();
    m_sniHost->registerHost();

    connect(UkuiQuick::ConfigLoader::getGlobalWidgetConfig("org.ukui.systemTray"), &UkuiQuick::WidgetConfig::configChanged, this, [=] {

        QStringList hidelist = m_settings->value(QStringLiteral("trayIconsInhibited")).toStringList();

        for (QString &itemId : hidelist) {
            if (!m_hideList.contains(itemId)) { //处理新增的隐藏
                m_hideList.append(itemId);
                QStringList sourceList = m_itemInfo.keys(itemId);
                for (QString &itemSource : sourceList) {
                    removeItem(itemSource);
                }
            }
        }

        for (QString itemId : m_hideList) {
            if (!hidelist.contains(itemId)) { //处理新增的显示
                m_hideList.removeOne(itemId);
                QStringList sourceList = m_itemInfo.keys(itemId);
                for (QString &itemSource : sourceList) {
                    addSource(itemSource);
                }
            }
        }
    });
    connect(m_sniHost, &UkuiSni::StatusNotifierHost::itemAdded, this, &TrayItemsModel::addSource);
    connect(m_sniHost, &UkuiSni::StatusNotifierHost::itemRemoved, this, &TrayItemsModel::removeSource);

    for (auto service : m_sniHost->services()) {
        addSource(service);
    }
}

int TrayItemsModel::rowCount(const QModelIndex &parent) const
{
    return m_item.length();
}

QVariant TrayItemsModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if ((i < 0) || (i >= m_item.length())) {
        return QVariant();
    }
    TrayItem* item = m_item[i];
    switch (static_cast<Role>(role)) {
        case Role::Service:
            return item->service();
        case Role::AttentionIcon:
            return item->attentionIcon();
        case Role::AttentionIconName:
            return item->attentionIconName();
        case Role::AttentionMovieName:
            return item->attentionMovieName();
        case Role::Category:
            return item->category();
        case Role::Icon:
            return item->icon();
        case Role::IconName:
            return item->iconName();
        case Role::IconThemePath:
            return item->iconThemePath();
        case Role::Id:
            return item->id();
        case Role::ItemIsMenu:
            return item->itemIsMenu();
        case Role::OverlayIcon:
            return item->overlayIcon();
        case Role::OverlayIconName:
            return item->overlayIconName();
        case Role::Status:
            return item->status();
        case Role::Title:
            return item->title();
        case Role::ToolTipSubTitle:
            return item->toolTipSubTitle();
        case Role::ToolTipTitle:
            return item->toolTipTitle();
        case Role::WindowId:
            return item->windowId();
        case Role::Fixed:
            return item->fixed();
        case Role::RecordOrder:
            return item->recordOrder();
        case Role::Row:
            return index.row();
        default:
            return {};
    }
}

QHash<int, QByteArray> TrayItemsModel::roleNames() const
{
    QHash<int, QByteArray> roles = QAbstractItemModel::roleNames();
    QMetaEnum e = metaObject()->enumerator(metaObject()->indexOfEnumerator("Role"));

    for (int i = 0; i < e.keyCount(); ++i) {
        roles.insert(e.value(i), e.key(i));
    }
    return roles;
}

void TrayItemsModel::activate(const QModelIndex &index)
{
    if (!index.isValid()) return;
    m_item.at(index.row())->activate(QCursor::pos().x(), QCursor::pos().y());
}

void TrayItemsModel::showContextMenu(const QModelIndex &index)
{
    if (!index.isValid()) return;
    m_item.at(index.row())->contextMenu(QCursor::pos().x(), QCursor::pos().y());
}

QVariant TrayItemsModel::extractIcon(const QIcon &icon, const QVariant &defaultValue)
{
    if (!icon.isNull()) {
        return icon;
    } else {
        return defaultValue;
    }
}

int TrayItemsModel::indexOfSource(const QString &source) const
{
    for (int i = 0; i < rowCount(QModelIndex()); i++) {
        if (m_item[i]->source() == source) {
            return i;
        }
    }
    return -1;
}

void TrayItemsModel::removeItem(const QString &source)
{
    int i = indexOfSource(source);
    if (i >= 0) {
        auto item = m_item.at(i);

        beginRemoveRows({}, i, i);
        m_item.removeAt(i);
        endRemoveRows();

        if (item) {
            item->disconnect();
            item->deleteLater();
        }
    }
    Q_EMIT dataChanged(index(i, 0, {}), index(m_item.size() - 1, 0, {}), {Role::Row});
}

void TrayItemsModel::setOrderInGroup(ItemGroupModel *group, const QModelIndex &groupIndex, int order)
{
    int end = group->groupEnd();
    int newOrder = group->groupBegin() + order;
    if(newOrder > end) {
        newOrder = end;
    }
    setOrder(group->mapToSource(groupIndex), newOrder);
}

void TrayItemsModel::setOrderBetweenGroups(ItemGroupModel *fromGroup, const QModelIndex &beginIndex, ItemGroupModel *toGroup, int order)
{
    int newOrder = toGroup->groupBegin() + order;
    if(newOrder > toGroup->groupEnd()) {
        newOrder = toGroup->groupEnd() + 1;
    }
    setOrder(fromGroup->mapToSource(beginIndex), newOrder);
}

void TrayItemsModel::changeSeparateIndex(bool add)
{
    if (m_separateIndex > m_item.length() - 1) {
        m_separateIndex = m_item.length() - 1;
    }
    add ? m_separateIndex ++ : m_separateIndex--;
    Q_EMIT separateIndexChanged();
    m_settings->setValue(QStringLiteral("separateIndex"), m_separateIndex);
}

void TrayItemsModel::setOrder(const QModelIndex &index, int order)
{
    int oldRow = index.row();
    if ((oldRow < 0) || (oldRow >= m_item.length())) {
        return;
    }
    auto item = m_item.at(oldRow);
    if(item->fixed()) {
        return;
    }
    if(oldRow == order) {
        return;
    }

    QString id  = m_item.at(index.row())->id();
    if(order > oldRow) {
        if(item->recordOrder()) {
            QString idBefore;
            for(int row = order; row > oldRow; --row) {
                if(m_item.at(row)->recordOrder()) {
                    idBefore = m_item.at(row)->id();
                    break;
                }
            }
            if(!idBefore.isEmpty()) {
                int targetOrder = m_orderList.indexOf(idBefore);
                m_orderList.insert(targetOrder, m_orderList.takeAt(m_orderList.indexOf(id)));
                m_settings->setValue(QStringLiteral("orderedItems"), QStringList(m_orderList.mid(m_fixList.size())));
            }
        }
        beginMoveRows(index.parent(), oldRow, oldRow, index.parent(), order + 1);
        m_item.insert(order, m_item.takeAt(oldRow));
        endMoveRows();
        Q_EMIT dataChanged(this->index(oldRow, 0), this->index(order, 0), {Role::Row});
    } else {
        if(item->recordOrder()) {
            QString idAfter;
            for(int row = order; row < oldRow; ++row) {
                if(m_item.at(row)->recordOrder()) {
                    idAfter = m_item.at(row)->id();
                    break;
                }
            }
            if(!idAfter.isEmpty()) {
                int targetOrder = m_orderList.indexOf(idAfter);
                m_orderList.insert(targetOrder, m_orderList.takeAt(m_orderList.indexOf(id)));
                m_settings->setValue(QStringLiteral("orderedItems"), QStringList(m_orderList.mid(m_fixList.size())));
            }
        }
        beginMoveRows(index.parent(), oldRow, oldRow, index.parent(), order);
        m_item.insert(order, m_item.takeAt(oldRow));
        endMoveRows();
        Q_EMIT dataChanged(this->index(order, 0), this->index(oldRow, 0), {Role::Row});
    }
}

int TrayItemsModel::getSeparateIndex()
{
    return m_separateIndex;
}

void TrayItemsModel::addSource(const QString &source)
{
    for (TrayItem* &trayItem : m_item) {
        if (trayItem->source() == source) {
            return;
        }
    }

    auto item = new TrayItem(source);
    connect(item, &TrayItem::itemReady, this, &TrayItemsModel::insertItem);
    if (item->id() != "") {
        insertItem(item);
    }
}

void TrayItemsModel::removeSource(const QString &source)
{
    m_itemInfo.remove(source);
    removeItem(source);
}

void TrayItemsModel::insertItem(TrayItem* item)
{
    if (m_item.contains(item)) return;
    m_itemInfo.insert(item->source(), item->id());
    if (m_hideList.contains(item->id())) {
        removeSource(item->source());
        return;
    }

    int order = m_item.size();
    for(const QString &id : m_orderList) {
        if(id == item->id()) {
            int index = m_orderList.indexOf(id);

            if(index <= m_fixList.size() - 1) {
                item->setFixed(true);
            } else {
                item->setRecordOrder(true);
            }
            //index为0表示第一个
            if(index == 0) {
                order = 0;
            } else if(index > 0){
                bool find = false;
                //寻找前一个item所在的位置，前一个item未注册时寻找再前一个的位置
                for(int preIndex = index - 1; preIndex >= 0 && !find; preIndex--) {
                    QString preId = m_orderList.at(preIndex);
                    for(int i = 0; i < m_item.size(); i++) {
                        if(m_item.at(i)->id() == preId) {
                            order = i + 1;
                            find = true;
                            break;
                        }
                    }
                }
                //找不到放到第一个
                if(!find) {
                    order = 0;
                }
            }
            break;
        }
    }
    beginInsertRows(QModelIndex(), order, order);
    m_item.insert(order, item);
    endInsertRows();
    Q_EMIT dataChanged(index(order, 0, {}), index(m_item.size() - 1, 0, {}), {Role::Row});

    connect(item, &TrayItem::itemDataChanged, this, &TrayItemsModel::dataUpdated);
    connect(item, &TrayItem::menuStateChanged, this, &TrayItemsModel::menuStateChanged);
}

void TrayItemsModel::dataUpdated(const QString &sourceName)
{
    for (TrayItem* &trayItem : m_item) {
        if (trayItem->source() == sourceName) {
            int i = indexOfSource(sourceName);

            if (i >= 0) {
                Q_EMIT dataChanged(index(i, 0, {}), index(i, 0, {}));
            }
            return;
        }
    }
}
