import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQml.Models 2.15

import org.ukui.systemTray 1.0
import org.ukui.quick.items 1.0 as UkuiItems
import org.ukui.quick.platform 1.0 as Platform

DropArea {
    property bool isEmpty: foldModel.count > 0 ? false : true
    property int sourceIndex
    signal foldAreaDragFinshed(int selectIndex)
    width: foldModel.count > 5 ? foldView.cellWidth * 5 + 8 : foldModel.count * foldView.cellWidth + 8
    height: (foldModel.count % 5) === 0 ?
                Math.floor((foldModel.count / 5)) * foldView.cellHeight + 8 : Math.floor((foldModel.count / 5) + 1) * foldView.cellHeight + 8

    UkuiItems.StyleBackground {
        anchors.fill: parent
        paletteRole: Platform.Theme.Window
        radius: Platform.Theme.maxRadius

        GridView {
            id: foldView
            property string idSelect: ""
            anchors.fill: parent
            anchors.topMargin: 4
            anchors.leftMargin: 4
            cellWidth: dropArea.scaleFactor * 5
            cellHeight: dropArea.scaleFactor * 5
            interactive: false

            moveDisplaced: Transition {
                NumberAnimation { properties: "x,y"; easing.type: Easing.OutQuad; duration: 200 }
                NumberAnimation { properties: "z"; to: 1; easing.type: Easing.OutQuad; duration: 1 }
            }

            model: DelegateModel {
                id: foldModel
                model: FoldModel

                delegate: DropArea {
                    property int vIndex: DelegateModel.itemsIndex
                    id: foldDropArea
                    width: foldView.cellWidth
                    height: foldView.cellHeight
                    clip: true

                    GridView.onAdd: SequentialAnimation {
                        NumberAnimation {target: foldDropArea; property: "scale"; from: 0; to: 1; duration: 250; easing.type: Easing.InOutQuad}
                    }

                    onDropped: {
                        if (drag.source.rootId !== dropArea) return;
                        drag.source.Drag.active = false;
                        ItemModel.setOrderInGroup(FoldModel, FoldModel.index(foldBase.sourceIndex, 0), drag.source.selectIndex);
                    }

                    onEntered: {
                        if (drag.source.rootId !== dropArea) return;
                        foldModel.items.move(drag.source.selectIndex, vIndex)
                    }
                    onExited: {
                        controlFold.isContain = false;
                    }

                    Binding { target: controlFold; property: "selectIndex"; value: vIndex}
                    UkuiItems.StyleBackground {
                        id: controlBaseFold
                        clip: true
                        useStyleTransparency: false
                        anchors.fill: parent
                        paletteRole: Platform.Theme.BrightText
                        alpha: controlFold.pressed && controlFold.isContain ? 0.1 : controlFold.isContain ? 0.05 : 0

                        radius: Platform.Theme.maxRadius
                        TrayIcon {
                            id: controlFold
                            property int selectIndex: 0
                            property bool isContain: false
                            property var rootId: dropArea
                            anchors.fill: parent
                            appStatus: model.Status
                            appAttentionMovieName: model.AttentionMovieName
                            appIcon: model.Icon
                            appAttentionIcon: model.AttentionIcon
                            appOverlayIcon: model.OverlayIcon

                            hoverEnabled: true
                            acceptedButtons: Qt.LeftButton | Qt.RightButton

                            Drag.active: drag.active
                            Drag.hotSpot.x: width / 2
                            Drag.hotSpot.y: height / 2
                            Drag.dragType: Drag.Automatic
                            Drag.onActiveChanged: {
                                if (Drag.active) {
                                    controlFold.opacity = 0;
                                    exited();
                                }
                            }
                            UkuiItems.Tooltip {
                                id: tooltip
                                anchors.fill: parent
                                mainText: model.ToolTipTitle.length == 0 ? model.Title : model.ToolTipTitle
                                posFollowCursor: false
                                location: {
                                    switch (dropArea.panelPos) {
                                        case UkuiItems.Types.Bottom:
                                        case UkuiItems.Types.Left:
                                        case UkuiItems.Types.Right:
                                            return UkuiItems.Dialog.BottomEdge;
                                        case UkuiItems.Types.Top:
                                            return UkuiItems.Dialog.TopEdge;
                                    }
                                }
                                margin: 6
                            }

                            onEntered: {
                                isContain = true;
                            }
                            onExited: {
                                isContain = false
                            }
                            onCanceled: {
                                isContain = false
                            }

                            onClicked: {
                                if (mouse.button === Qt.LeftButton) {
                                    foldModel.model.onActivate(foldModel.modelIndex(index));
                                }
                                if (mouse.button === Qt.RightButton) {
                                    foldModel.model.onContextMenu(foldModel.modelIndex(index));
                                }
                            }

                            onPressed: {
                                if (mouse.button === Qt.LeftButton) {
                                    x = controlFold.mapToItem(foldView,0,0).x;
                                    y = controlFold.mapToItem(foldView,0,0).y;
                                    drag.target = controlFold;
                                    controlFold.grabToImage(function(result) {
                                        controlFold.Drag.imageSource = result.url;
                                    })
                                    foldBase.sourceIndex = vIndex;
                                    dropArea.dragType = 1;
                                }
                            }
                            onReleased: {
                                Drag.drop();
                                drag.target = null;
                                controlFold.parent = controlBaseFold;
                                controlFold.opacity = 1
                                x = 0;
                                y = 0;
                            }
                            Drag.onDragFinished: {
                                Drag.active = false;
                                foldAreaDragFinshed(foldBase.sourceIndex);
                            }
                        }
                    }
                }
            }
        }
    }
}
