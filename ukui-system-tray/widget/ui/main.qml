import QtQuick 2.15
import QtQuick.Layouts 1.15
import org.ukui.quick.widgets 1.0
import org.ukui.quick.items 1.0

WidgetItem {
    property bool panelOrientation: (Widget.orientation === Types.Horizontal)
    property int pos: Widget.container.position
    Layout.fillWidth: panelOrientation ? false : true
    Layout.fillHeight: panelOrientation ? true : false
    Layout.preferredWidth: childrenRect.width
    Layout.preferredHeight: childrenRect.height
    clip: true
    Widget.active: dropArea.panelActive

    TrayView {
        id: dropArea
        property bool panelActive: false
        scaleFactor: parent.panelOrientation ? (parent.height / 44) * 8 : (parent.width / 44) * 8
        itemOrientation: parent.panelOrientation
        itemWidth: parent.panelOrientation ? scaleFactor * 4 : scaleFactor * 5
        itemHeight: parent.panelOrientation ? scaleFactor * 5 : scaleFactor * 4
        itemLong: scaleFactor * 5
        itemShort: scaleFactor * 4
        panelPos: parent.pos
    }

    onWidthChanged: {
        dropArea.changeFoldWindowPosition();
    }

    onHeightChanged: {
        dropArea.changeFoldWindowPosition();
    }
}
