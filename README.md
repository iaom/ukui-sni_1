# ukui-sni

#### 介绍
ukui-sni是ukui桌面环境中系统托盘服务相关功能的集合。
托盘功能基于[ukui4托盘协议](https://yayqg9ul2g.feishu.cn/docx/Q1Ptd8S0VoKryFxUQFxcbRS4n4g)。
#### status-notifier-host
用于实现可视化托盘区域功能的接口。
#### status-notifier-watcher
sni协议中的Watcher服务实现。
#### ukui-system-tray
可视化托盘插件（会显示在任务栏上）